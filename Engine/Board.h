#pragma once
#include"Graphics.h"
#include"Location.h"
#include"Colors.h"



class Board
{
public:

	Board ( Graphics& gfx );
	void Draw(Location& loc ,Color c );
	int GetWidth();
	int GetHeight();

private:

	Graphics& gfx; 
	static constexpr int  dimension = 20;
	static constexpr int width=10;
	static constexpr int height = 10;



};

