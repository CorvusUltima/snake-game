#pragma once
#include"Board.h"
class Snake
{

private:class Segment
{

public:

	void initHead(const Location& loc_in);
	void initBody();
	void Folow(const Segment& next);
	void MoveBy(Location& delta_loc);
	void Draw(Board& brd);
private:
	Location loc;
	Color c;
};
public:
	Snake(const Location& loc);
	void MoveBy(Location& delta_loc);
	void Grow();
	void Draw(Board&brd);
private:
	static constexpr Color headColor = Colors::Yellow;
	static constexpr Color bodyColor = Colors::Green;
	static constexpr  int nSegmentsMax = 100;
	Segment segments[nSegmentsMax];
	int nSegments = 1;
};

