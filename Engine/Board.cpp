#include "Board.h"

Board::Board(Graphics& gfx)
	:
gfx(gfx)
{
}

void Board::Draw(Location& loc, Color c)
{
	gfx.DrawRectDim(loc.x * dimension, loc.y * dimension, dimension, dimension, c);
}

int Board::GetWidth()
{
	return width;
}

int Board::GetHeight()
{
	return height;
}
