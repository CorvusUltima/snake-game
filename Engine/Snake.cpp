#include "Snake.h"
#include<assert.h>

Snake::Snake(const Location& loc)
{
	segments[0].initHead(loc);
}

void Snake::MoveBy(Location& delta_loc)
{
	for (int i = nSegments - 1; i > 0;--i)
	{
		segments[i].Folow(segments[i - 1]);
	}

	segments[0].MoveBy(delta_loc);
}

void Snake::Grow()
{
	if (nSegments < nSegmentsMax)
	{
		nSegments++;
	}
}

void Snake::Draw(Board& brd) 
{
	for (int i = 0; i < nSegments; i++)
	{
		segments[i].Draw(brd);
	}
}

void Snake::Segment::initHead(const Location& loc_in)
{
	loc = loc_in;
	c = Snake::headColor;

}

void Snake::Segment::initBody()
{
	c = Snake::bodyColor;
}

void Snake::Segment::Folow(const Segment& next)
{
	loc = next.loc;
}

void Snake::Segment::MoveBy(Location& delta_loc)
{
	assert (abs(delta_loc.x) + abs(delta_loc.y )== 1);
	loc.Add(delta_loc);
}

void Snake::Segment::Draw(Board& brd) 
{
	brd.Draw(loc, c);
}
